require("dotenv").config();
const nodemailer = require("nodemailer");

// 設定 nodemailer 的傳輸器
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.Email_User, // 使用環境變數中的發件人郵件
    pass: process.env.Email_Password, // 使用環境變數中的密碼
  },
});

// 設定郵件選項
const mailOptions = {
  from: process.env.Email_User, // 發件人
  to: "newaloo31124@gmail.com", // 收件人，可以是其他郵件地址
  subject: "Test Email from Node.js",
  text: "Hello, this is a test email sent from Node.js using Nodemailer",
};

// 發送郵件
transporter.sendMail(mailOptions, (error, info) => {
  if (error) {
    return console.log(`Error occurred:`, error);
  }
  console.log(`Email sent successfully:`, info.response);
});
