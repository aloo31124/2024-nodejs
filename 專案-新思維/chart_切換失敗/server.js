

const http = require('http');
const controller = require('./controller');

const server = http.createServer((req, res) => {
  if (req.method === 'GET' && req.url === '/') {
    controller.showIndexPage(req, res);
  } else if (req.method === 'GET' && req.url === '/api/revenue') {
    controller.getRevenueData(req, res);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

server.listen(8081, () => {
  console.log('Server is running on http://localhost:8081');
});
