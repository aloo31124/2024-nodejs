
// 假資料生成 - dataModel.js

exports.generateRevenueData = () => {
    const months = ['January', 'February', 'March'];
    const data = {
      companyA: [],
      companyB: [],
    };
  
    months.forEach(month => {
      // 生成每月隨機營收數據 (單位：千元)
      data.companyA.push({
        month,
        revenue: Math.floor(Math.random() * 100) + 50,
      });
      data.companyB.push({
        month,
        revenue: Math.floor(Math.random() * 100) + 50,
      });
    });
  
    return data;
  };
  
