


const fs = require('fs');
const path = require('path');
const dataModel = require('./dataModel');

// 顯示主頁
exports.showIndexPage = (req, res) => {
  fs.readFile(path.join(__dirname, 'views', 'index.html'), (err, data) => {
    if (err) {
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end('Server Error');
    } else {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(data);
    }
  });
};

// API 端點提供假數據
exports.getRevenueData = (req, res) => {
  const data = dataModel.generateRevenueData();
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify(data));
};

