
const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json'); // 下載的 Firebase 管理員密鑰檔案

// 初始化 Firebase
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();
module.exports = db;

