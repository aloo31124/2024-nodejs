
const fs = require('fs');
const db = require('../firebase');

// 顯示註冊頁面
/* 
exports.showRegisterPage = (req, res) => {
    res.sendFile(__dirname + '/../views/register.html');
};
 */
exports.showRegisterPage = (req, res, errorMessage = '') => {
    fs.readFile('./views/register.html', (err, data) => {
        if(err) {
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end(' 顯示註冊 錯誤 ');
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});
            const pageContent = data.toString().replace('{{errorMessage}}', errorMessage);
            res.end(pageContent);
        }
    });
};

// 處理註冊邏輯
exports.handleRegister = async (req, res) => {
    const { username, name } = req.body;

    try {
        // 將資料存至 Firestore 的 User 表
        await db.collection('User').add({
            username,
            name,
            createdAt: new Date(),
        });

        res.send('<h1>註冊成功！</h1><a href="/register">返回註冊頁面</a>');
    } catch (error) {
        console.error('Error writing to Firestore:', error);
        res.status(500).send('<h1>註冊失敗！</h1><a href="/register">返回註冊頁面</a>');
    }
};

