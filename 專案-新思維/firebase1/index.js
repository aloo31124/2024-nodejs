const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/user.routes');

const app = express();

// 中間件
app.use(bodyParser.urlencoded({ extended: true }));

// 路由
app.use(userRoutes);

// 啟動伺服器
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});

