
const express = require('express');
const userController = require('../controllers/user.controller');

const router = express.Router();

// 註冊頁面路由
router.get('/register', userController.showRegisterPage);

// 註冊邏輯路由
router.post('/register', userController.handleRegister);

module.exports = router;


