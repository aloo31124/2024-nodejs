// 伺服器入口，增加登出路由處理

const http = require('http');
const authController = require('./controllers/authController');

const server = http.createServer((req, res) => {
  if (req.url === '/login' && req.method === 'GET') {
    authController.showLoginPage(req, res);
  } else if (req.url === '/login' && req.method === 'POST') {
    authController.handleLogin(req, res);
  } else if (req.url === '/home' && req.method === 'GET') {
    authController.showHomePage(req, res);
  } else if (req.url === '/logout' && req.method === 'POST') {
    authController.handleLogout(req, res);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Page Not Found');
  }
});

server.listen(8081, () => {
  console.log('Server is running on http://localhost:8081/login');
});

