// 控制器，處理登入、登出和 token 驗證

const fs = require('fs');
const querystring = require('querystring');
const jwt = require('../utils/jwt');
const userModel = require('../models/userModel');

exports.showLoginPage = (req, res, errorMessage = '') => {
  fs.readFile('./views/login.html', (err, data) => {
    if (err) {
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end('Server Error');
    } else {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      const pageContent = data.toString().replace('{{errorMessage}}', errorMessage);
      res.end(pageContent);
    }
  });
};

exports.handleLogin = (req, res) => {
  let body = '';
  req.on('data', chunk => { body += chunk; });
  req.on('end', () => {
    const { username, password } = querystring.parse(body);
    const user = userModel.validateUser(username, password);

    if (user) {
      const token = jwt.generateToken({ username }, '5m'); // 設置5分鐘有效期
      res.writeHead(302, {
        'Set-Cookie': `token=${token}; HttpOnly`,
        'Location': '/home'
      });
      res.end();
    } else {
      exports.showLoginPage(req, res, '帳號或密碼錯誤');
    }
  });
};

exports.showHomePage = (req, res) => {
  const token = req.headers.cookie && req.headers.cookie.split('=')[1];
  if (token && jwt.verifyToken(token)) {
    fs.readFile('./views/home.html', (err, data) => {
      if (err) {
        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end('Server Error');
      } else {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(data);
      }
    });
  } else {
    res.writeHead(302, { 'Location': '/login' });
    res.end();
  }
};

exports.handleLogout = (req, res) => {
  res.writeHead(302, {
    'Set-Cookie': 'token=; HttpOnly; Max-Age=0',
    'Location': '/login'
  });
  res.end();
};
