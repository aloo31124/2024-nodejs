//修改 token 有效期的生成方式

const jwt = require('jsonwebtoken');
const SECRET_KEY = 'your_secret_key';

exports.generateToken = (payload, expiresIn = '5m') => {
  return jwt.sign(payload, SECRET_KEY, { expiresIn });
};

exports.verifyToken = (token) => {
  try {
    return jwt.verify(token, SECRET_KEY);
  } catch (err) {
    return null;
  }
};

