
const users = [
    { id: 1, name: "Alice", age: 25 },
    { id: 2, name: "Bob", age: 30 },
    { id: 3, name: "Charlie", age: 35 }
  ];
  
  const getUsers = () => users;
  
  const getUserById = (id) => users.find(user => user.id === id);
  
  const addUser = (user) => {
    const newUser = { id: users.length + 1, ...user };
    users.push(newUser);
    return newUser;
  };
  
  module.exports = { getUsers, getUserById, addUser };
  