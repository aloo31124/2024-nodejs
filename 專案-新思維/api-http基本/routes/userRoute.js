// routes/userRoutes.js

const userController = require('../controllers/userController');

const userRoutes = (req, res) => {
  if (req.url === '/users' && req.method === 'GET') {
    userController.getUsers(req, res);
  } else if (req.url.match(/\/users\/\d+/) && req.method === 'GET') {
    const id = parseInt(req.url.split('/')[2], 10);
    userController.getUserById(req, res, id);
  } else if (req.url === '/users' && req.method === 'POST') {
    userController.addUser(req, res);
  } else {
    res.writeHead(404, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ message: "Route not found" }));
  }
};

module.exports = userRoutes;
