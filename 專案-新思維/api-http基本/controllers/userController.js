// controllers/userController.js

const userModel = require('../models/userModel');

const getUsers = (req, res) => {
  const users = userModel.getUsers();
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify(users));
};

const getUserById = (req, res, id) => {
  const user = userModel.getUserById(id);
  if (user) {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(user));
  } else {
    res.writeHead(404, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ message: "User not found" }));
  }
};

const addUser = (req, res) => {
  let body = '';
  req.on('data', chunk => {
    body += chunk.toString();
  });
  req.on('end', () => {
    const { name, age } = JSON.parse(body);
    const newUser = userModel.addUser({ name, age });
    res.writeHead(201, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(newUser));
  });
};

module.exports = { getUsers, getUserById, addUser };
