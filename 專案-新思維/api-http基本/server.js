// server.js

const http = require('http');
const userRoutes = require('./routes/userRoute');

const server = http.createServer((req, res) => {
  userRoutes(req, res);
});

server.listen(8080, () => {
  console.log("Server is running on http://localhost:8080");
});
