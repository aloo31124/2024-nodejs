

// 在 controllers 文件夾下，創建一個 authController.js 文件，處理登入驗證並生成 JWT token。
const jwt = require('jsonwebtoken');
const user = require('../models/userModel');

exports.login = (req, res) => {
  let body = '';
  req.on('data', chunk => { body += chunk.toString(); });
  
  req.on('end', () => {
    const { username, password } = JSON.parse(body);

    if (username === user.username && password === user.password) {
      const token = jwt.sign({ username }, process.env.JWT_SECRET, { expiresIn: '5m' });
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify({ token }));
    } else {
      res.writeHead(401, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify({ message: 'Invalid credentials' }));
    }
  });
};
