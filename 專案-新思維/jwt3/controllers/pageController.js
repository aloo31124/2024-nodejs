

// 在 controllers 文件夾下，創建一個 pageController.js 文件，用於回應不同頁面請求。
const fs = require('fs');
const path = require('path');

exports.servePage = (page, res) => {
  fs.readFile(path.join(__dirname, '../views', `${page}.html`), (err, data) => {
    if (err) {
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end('Server Error');
    } else {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.end(data);
    }
  });
};


