
// 在 utils 文件夾下，創建一個 authMiddleware.js 文件，負責驗證 JWT token，並實現 Token 的過期處理。
const jwt = require('jsonwebtoken');

exports.verifyToken = (req, res, next) => {
  const token = req.headers['authorization'];

  if (!token) {
    res.writeHead(302, { Location: '/login' });
    res.end();
    return;
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      res.writeHead(302, { Location: '/login' });
      res.end();
      return;
    }
    req.user = decoded;
    next();
  });
};

