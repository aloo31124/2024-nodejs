

// 設置 HTTP 伺服器，並在不同的路徑中加入路由來處理頁面導向、登入請求和驗證。
require('dotenv').config();
const http = require('http');
const authController = require('./controllers/authController');
const pageController = require('./controllers/pageController');
const authMiddleware = require('./utils/authMiddleware');

const server = http.createServer((req, res) => {
  if (req.method === 'GET') {
    if (req.url === '/') {
      res.writeHead(302, { Location: '/home' });
      res.end();
    } else if (req.url === '/login') {
      pageController.servePage('login', res);
    } else if (req.url === '/home') {
      authMiddleware.verifyToken(req, res, () => pageController.servePage('home', res));
    } else if (req.url === '/chart') {
      authMiddleware.verifyToken(req, res, () => pageController.servePage('chart', res));
    }
  } else if (req.method === 'POST' && req.url === '/login') {
    authController.login(req, res);
  } else if (req.method === 'POST' && req.url === '/logout') {
    res.writeHead(302, { Location: '/login', 'Set-Cookie': 'token=; Max-Age=0' });
    res.end();
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

server.listen(8081, () => {
  console.log('Server running at http://localhost:8081');
});
