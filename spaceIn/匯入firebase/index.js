
const express = require('express');
const multer = require('multer');
const path = require('path');
const testRoomController = require('./controllers/testRoom.controller');

const app = express();
const upload = multer({ dest: 'uploads/' }); // 上傳暫存目錄

// 靜態檔案
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));

// 路由
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'views/index.html')));
app.get('/download-template', testRoomController.downloadTemplate);
app.post('/import-csv', upload.single('file'), testRoomController.importCSV);

// 啟動伺服器
const PORT = 8081;
app.listen(PORT, () => {
    console.log(`伺服器啟動於 http://localhost:${PORT}`);
});

