const admin = require('firebase-admin');
const serviceAccount = require('../serviceAccountKey.json'); // 請確保此檔案存在並正確配置

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();
module.exports = db;


