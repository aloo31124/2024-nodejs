
const fs = require('fs');
const path = require('path');
const csvParser = require('csv-parser');
const db = require('../models/firestore');

// 下載範本 CSV
exports.downloadTemplate = (req, res) => {
    const filePath = path.join(__dirname, '../public/template.csv');
    res.download(filePath, 'TestRoomTemplate.csv');
};

// 匯入 CSV
exports.importCSV = (req, res) => {
    const file = req.file; // 透過 Multer 上傳的檔案
    if (!file) {
        return res.status(400).send('請選擇 CSV 檔案');
    }

    const results = [];
    fs.createReadStream(file.path)
        .pipe(csvParser()) // 使用 csv-parser 解析 CSV
        .on('data', (row) => {
            // 篩選欄位，確保符合 Firestore 格式
            if (row.name && row.numberOfPeople) {
                results.push({
                    name: row.name,
                    numberOfPeople: parseInt(row.numberOfPeople, 10),
                    createDate: new Date(),
                });
            }
        })
        .on('end', async () => {
            try {
                const batch = db.batch();
                const testRoomRef = db.collection('TestRoom');

                results.forEach((room) => {
                    const docRef = testRoomRef.doc(); // 自動生成 ID
                    batch.set(docRef, room);
                });

                await batch.commit();
                res.send('CSV 匯入成功！');
            } catch (error) {
                console.error(error);
                res.status(500).send('匯入時發生錯誤');
            }
        });
};


